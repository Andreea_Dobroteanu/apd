
						Tema 1 APD
					   Game of Life

				-------------------------
				Dobroteanu Andreea, 331CA


-------------------------------------------------------------------------------

Implementare:
    - Citesc datele necesare din fisier si argumentele primite din linia de
        comanda;

    - Cat timp mai sunt iteratii de parcurs:
        - Bordez matricea:
            - Atribui valorile pentru colturi
            - Pentru atribuirea marginilor, am paralelizat; variabilele
                folosite sunt separate, deci ordinea executiei poate fi
                aleatoare
        - Calcularea celulelor si transferul din matricea auxiliara in cea initiala:
            - Operatiile sunt paralelizate
            - Pentru calculul celulelor ocupate, am paralelizat cele doua
                bucle, unindu-le un una singura (collapse), si am pastrat
                variabila count ca si private (fiecare thread ar trebui sa aiba
                propria versiune a counterului)
            - Counterului ii atribui numarul de celule vecine ocupate,
                ii verific apoi valoarea si adaug celula corecta in
                matricea auxiliara.
            - Pun o bariera inainte de a copia valorile din matricea auxiliara
                in cea initiala, pentru a astepta ca toate thread-urile
                sa termine de completat matricea.
            - Copiez matricea nou obtinuta in cea initiala, tot paralel
            - Decrementez numarul de iteratii

    - Scriu matricea obtinuta in fisierul de iesire
