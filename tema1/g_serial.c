#include <stdio.h>
#include <stdlib.h>

/**
 * Bordare matrice
 * @param matrix [char**] matricea care trebuie bordata
 * @param N  [int] numar linii
 * @param M  [int] numar coloane
 */
void borderMatrix(char **matrix, int N, int M) {
    /**
     * Colturi
     */
    matrix[0][0] = matrix[N][M];
    matrix[0][M + 1] = matrix[N][1];
    matrix[N + 1][0] = matrix[1][M];
    matrix[N + 1][M + 1] = matrix[1][1];

    int l;
    /**
     * Margini sus/jos
     */
    for (l = 1; l <= M; l++) {
        matrix[0][l] = matrix[N][l];
        matrix[N + 1][l] = matrix[1][l];
    }

    /**
     * Margini stanga/dreapta
     */
    for (l = 1; l <= N; l++) {
        matrix[l][0] = matrix[l][M];
        matrix[l][M + 1] = matrix[l][1];
    }

}

int main(int argc, char *argv[]) {
    // Numar linii / coloane / iteratii
    int N, M, K;
    int i, j;
    // Fisiere intrare/iesire
    FILE *fIn, *fOut;

    if (argc < 4) {
        fprintf(stderr, "Usage: %s in.txt <iterations> out.txt\n", argv[0]);
        return 0;
    }

    // Nmarul de iteratii primit ca parametru
    K = atoi(argv[2]);

    fIn = fopen(argv[1], "r");

    // Citire numar linii/coloane
    fscanf(fIn, "%d", &N);
    fscanf(fIn, "%d", &M);

    // Alocare matrice de lucru / matrice auxiliara
    char **matrix = (char **)malloc((N + 2) * sizeof(char *));
    char **auxMatrix = (char **)malloc((N + 2) * sizeof(char *));
    for (i = 0; i < (N + 2); i++) {
        matrix[i] = (char *)malloc((M + 2) * sizeof(char));
        auxMatrix[i] = (char *)malloc((M + 2) * sizeof(char));
    }

    // variabile auxiliare pentru citire spatii/linii noi
    char buff[101], space;
    fgets(buff, 100, fIn);

    // Citire matrice
    for (i = 1; i <= N; i++) {
        for (j = 1; j <= M; j++) {
            fscanf(fIn, "%c", &matrix[i][j]);
            fscanf(fIn, "%c", &space);
        }

        fgets(buff, 100, fIn);
     }

    fclose(fIn);

    int count;  // counter pentru numarul de vecini
    while (K > 0) {
        // bordare matrice
        borderMatrix(matrix, N, M);
        for (i = 1; i <= N; i++) {
            for (j = 1; j <= M; j++) {
                //	Numarare vecini
                count = (matrix[i][j - 1] == 'X') +
                        (matrix[i][j + 1] == 'X') +
                        (matrix[i - 1][j] == 'X') +
                        (matrix[i + 1][j] == 'X') +
                        (matrix[i - 1][j - 1] == 'X') +
                        (matrix[i - 1][j + 1] == 'X') +
                        (matrix[i + 1][j - 1] == 'X') +
                        (matrix[i + 1][j + 1] == 'X');

                /**
                 * Adaugare celule in matricea auxiliara, in functie
                 *  de numarul de vecini
                 */
                switch (count) {
                    //  2 vecini -> celula ramane la fel
                    case 2:
                        auxMatrix[i][j] = matrix[i][j];
                        break;
                    //  3 vecini -> celula este locuita
                    case 3:
                        auxMatrix[i][j] = 'X';
                        break;
                    //  Altfel -> celula nu esta locuita (nu a fost niciodata/este moarta)
                    default:
                        auxMatrix[i][j] = '.';
                        break;
                }
            }
        }

        //	Transfer din matricea auxiliara in matricea initiala
        for (i = 0; i <= N + 1; i++)
            for (j = 0; j <= M + 1; j++)
                matrix[i][j] = auxMatrix[i][j];

        K--;
    }

    fOut = fopen(argv[3], "w");

    /**
     * Scriere matrice finala in fisierul de iesire
     */
    for (i = 1; i <= N; i++) {
        for (j = 1; j <= M; j++) {
            fprintf(fOut, "%c ", matrix[i][j]);
        }

        fputs("\n", fOut);
    }

    fclose(fOut);

    /**
     * Eliberare memorie
     */
    for (i = 0; i < N + 2; i++) {
        char* p = matrix[i];
        free(p);
    }

    for (i = 0; i < N + 2; i++) {
        char* p = auxMatrix[i];
        free(p);
    }

    return 0;
}