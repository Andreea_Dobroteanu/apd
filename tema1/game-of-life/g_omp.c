#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
//#define MAX 1005

void borderMatrix(char **Mat, int N, int M) {
    //	Pentru matrice toroidala
    //	Cazuri margini:
    //	Colturi:
    Mat[0][0] = Mat[N][M];
    Mat[0][M + 1] = Mat[N][1];
    Mat[N + 1][0] = Mat[1][M];
    Mat[N + 1][M + 1] = Mat[1][1];

    int l;
    //	Sus/Jos:
    #pragma omp parallel for
    for (l = 1; l <= M; l++) {
        Mat[0][l] = Mat[N][l];
        Mat[N + 1][l] = Mat[1][l];
    }

    //	Stanga/Dreapta:
    #pragma omp parallel for
    for (l = 1; l <= N; l++) {
        Mat[l][0] = Mat[l][M];
        Mat[l][M + 1] = Mat[l][1];
    }

}

int main(int argc, char *argv[]) {
	int M, N, K; //latimea & lungimea & interatii
	int i, j;

	if (argc < 4) {
		fprintf(stderr, "Usage: %s in.txt <iterations> out.txt\n", argv[0]);
		return 0;
	}

	K = atoi(argv[2]);
    FILE *fIn, *fOut;

    fIn = fopen(argv[1], "r");

    fscanf(fIn, "%d", &N);
    fscanf(fIn, "%d", &M);

    char **Mat = (char **)malloc((N + 2) * sizeof(char *));
    char **M_aux = (char **)malloc((N + 2) * sizeof(char *));
    for (i = 0; i < (N + 2); i++) {
        Mat[i] = (char *)malloc((M + 2) * sizeof(char));
        M_aux[i] = (char *)malloc((M + 2) * sizeof(char));
    }

    char buff[101], space;
    fgets(buff, 100, fIn);

    for (i = 1; i <= N; i++) {
        for (j = 1; j <= M; j++) {
            fscanf(fIn, "%c", &Mat[i][j]);
            fscanf(fIn, "%c", &space);
        }

        fgets(buff, 100, fIn);
    }

    fclose(fIn);


    omp_set_num_threads(4);
//    omp_set_num_threads(1);

    int count;
	while (K > 0) {
        borderMatrix(Mat, N, M);
        #pragma omp parallel for private(count) collapse(2)
		for (i = 1; i <= N; i++) {
            for (j = 1; j <= M; j++) {
                count = 0;
                //	Cazuri vecini:
                if (Mat[i][j - 1] == 'X') {
                    count++;
                }
                if (Mat[i][j + 1] == 'X') {
                    count++;
                }
                if (Mat[i - 1][j] == 'X') {
                    count++;
                }
                if (Mat[i + 1][j] == 'X') {
                    count++;
                }
                if (Mat[i - 1][j - 1] == 'X') {
                    count++;
                }
                if (Mat[i - 1][j + 1] == 'X') {
                    count++;
                }
                if (Mat[i + 1][j - 1] == 'X') {
                    count++;
                }
                if (Mat[i + 1][j + 1] == 'X') {
                    count++;
                }

                //	Daca numar vecini...
                switch (count) {
                    case 2:
                        M_aux[i][j] = Mat[i][j];
                        break;
                    case 3:
                        M_aux[i][j] = 'X';
                        break;
                    default:
                        M_aux[i][j] = '.';
                        break;
                }
            }
        }

        //	Transfer din matricea auxiliara in matricea initiala
        for (i = 0; i < N; i++)
            for (j = 0; j < M; j++)
                Mat[i][j] = M_aux[i][j];

        K--;
	}

    fOut = fopen(argv[3], "w");

    for (i = 1; i <= N; i++) {
        for (j = 1; j <= M; j++) {
            fprintf(fOut, "%c ", M_aux[i][j]);
        }

        fputs("\n", fOut);
    }

    fclose(fOut);

    for (i = 0; i < N + 2; i++) {
        char* p = Mat[i];
        free(p);
    }

    for (i = 0; i < N + 2; i++) {
        char* p = M_aux[i];
        free(p);
    }

    return 0;
}