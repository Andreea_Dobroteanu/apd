#include <stdio.h>
#define MAX 1005

int main() {
	int Mat[MAX][MAX], M_aux[MAX][MAX];
	char tip; // P | T
	int M, N, K; //latimea & lungimea & interatii
	int i, j;

	scanf ("%c %d %d %d", &tip, &M, &N, &K);
	for (i = 0; i < N; i++)
		for (j = 0; j < M; j++)
			scanf("%d", &Mat[i][j]);

	int pop, pop_max; //pentru bonus

	while (K > 0) {
		int count, pop = 0;
		for (i = 0; i < N; i++) {
			for (j = 0; j < M; j++) {
				count = 0;
				//	Pentru matrice toroidala
				if (tip == 'T') {
				//	Cazuri margini:
				//	Colturi:
					Mat[-1][-1] = Mat[N-1][M-1];
					Mat[-1][M] = Mat[N-1][0];
					Mat[N][-1] = Mat[0][M-1];
					Mat[N][M] = Mat[0][0];
				//	Sus/Jos:
					int l;
					for (l = 0; l < M; l++) {
						Mat[-1][l] = Mat[N-1][l];
						Mat[N][l] = Mat[0][l];
					}
				//	Stanga/Dreapta:
					for (l = 0; l < N; l++) {
						Mat[l][-1] = Mat[l][M-1];
						Mat[l][M] = Mat[l][0];
					}
				}

			//	Cazuri vecini:
				if (Mat[i][j-1] == 1)
					count++;
				if (Mat[i][j+1] == 1)
					count++;
				if (Mat[i-1][j] == 1)
					count++;
				if (Mat[i+1][j] == 1)
					count++;
				if (Mat[i-1][j-1] == 1)
					count++;
				if (Mat[i-1][j+1] == 1)
					count++;
				if (Mat[i+1][j-1] == 1)
					count++;
				if (Mat[i+1][j+1] == 1)
					count++;

			//	Daca numar vecini...
				switch(count) {
					case 2:
						M_aux[i][j] = Mat[i][j];
						break;
					case 3:
						M_aux[i][j] = 1;
						break;
					default:
						M_aux[i][j] = 0;
						break;
				}
			//	Grad de populare:
				if (Mat[i][j] == 1)
						pop++;
				}
		}

		//	Transfer din matricea auxiliara in matricea initiala
		for (i = 0; i < N; i++)
			for (j = 0; j < M; j++)
				Mat[i][j] = M_aux[i][j];

		if (pop > pop_max)
			pop_max = pop;
		
		K--;
	}

	for (i = 0; i < N; i++) {
		for (j = 0; j < M; j++)
			printf("%d ", M_aux[i][j]);
		printf("\n");
	}

	printf("%.3f%%\n", 100*(float)pop_max/(N*M));
}