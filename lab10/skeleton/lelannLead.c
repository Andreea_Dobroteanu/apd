#include <mpi.h>
#include <stdio.h>

int main(int argc, char * argv[]) {
    int rank;
    int nProcesses;
    int v;
    MPI_Init(&argc, &argv);
    MPI_Status stat;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nProcesses);
    printf("Hello from %i/%i\n", rank, nProcesses);

    int crtProcess = rank - 1, leader = -1;

    if(rank == 0) {
        crtProcess = nProcesses - 1;
    }

    while(crtProcess != rank) {
        if(rank == nProcesses - 1) {
            MPI_Send(&crtProcess, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
        } else {
            MPI_Send(&crtProcess, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);
        }

        if(rank == 0) {
            MPI_Recv(&crtProcess, 1 , MPI_INT, nProcesses - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        } else {
            MPI_Recv(&crtProcess, 1 , MPI_INT, rank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
        }

        if(crtProcess > leader) {
            leader = crtProcess;
        }
    }

    if (rank == 0) {
        printf("Process: %d <-> Leader: %d\n", rank, leader);
    } else {
        printf("Process: %d <-> Leader: %d\n", rank, nProcesses - 1);
    }

    MPI_Finalize();
    return 0;
}

