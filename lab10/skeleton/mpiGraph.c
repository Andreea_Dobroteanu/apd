#include<mpi.h>
#include<stdio.h>

/**
 * @author cristian.chilipirea
 * Run: mpirun -np 10 ./a.out
 */

/**
 * { node, neighbour }
 */
int graph[][2] = { { 0, 1 }, { 0, 4 }, { 0, 5 }, 
                  { 1, 0 }, { 1, 2 }, { 1, 6 }, 
                  { 2, 1 }, { 2, 3 }, { 2, 7 },
                  { 3, 2 }, { 3, 4 }, { 3, 8 },
                  { 4, 0 }, { 4, 3 }, { 4, 9 },
                  { 5, 0 }, { 5, 7 }, { 5, 8 },
                  { 6, 1 }, { 6, 8 }, { 6, 9 },
                  { 7, 2 }, { 7, 5 }, { 7, 9 },
                  { 8, 3 }, { 8, 5 }, { 8, 6 },
                  { 9, 4 }, { 9, 6 }, { 9, 7 } };


const int NODES = 3;

int maxv(int v[NODES]) {
	int max = -1, i = 0;
	for (i = 0; i < NODES; i++) {
		if (max < v[i]) {
			max = v[i];
		}
	}
	return max;
}

int main(int argc, char * argv[]) {
	int rank;
	int nProcesses;

	MPI_Init(&argc, &argv);
	MPI_Status status;
	MPI_Request request;


	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nProcesses);
	printf("Hello from %i/%i\n", rank, nProcesses);

	int i, n, leader;
	for(i = 0; i < 1000; i++){
		for (n = 0; n < NODES; n++) {
			MPI_Send(&graph[rank * NODES][0], 1, MPI_INT, graph[rank * NODES + n][1], 1, MPI_COMM_WORLD);
		}

		int recv[NODES];

		for (n = 0; n < NODES; n++) {
			MPI_Recv(&recv[n], 1, MPI_INT, graph[rank * NODES + n][1], 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}

		int maxRecv = maxv(recv);
		graph[rank * NODES][0] = graph[rank * NODES][0] < maxRecv ? maxRecv : graph[rank * NODES][0];

		leader = graph[rank * NODES][0];
	}

	printf("Leader: %d\n", leader);
	printf("Bye from %i/%i\n", rank, nProcesses);
	MPI_Finalize();
	return 0;
}