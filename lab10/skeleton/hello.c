#include<mpi.h>
#include<stdio.h>
#include<string.h>

int main(int argc, char * argv[]) {
    int rank;
    int nProcesses;

    MPI_Init(&argc, &argv);
    MPI_Status status;
    MPI_Request isReq, irReq;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nProcesses);

    char* message = "Hello";
    int length = strlen(message) + 1;
    char buffer[length];

    printf("Send: %s\n", message);
    /**
     *  int MPI_Isend(const void *buf, int count, MPI_Datatype datatype, int dest,
     *                int tag, MPI_Comm comm, MPI_Request *request)
     */
    MPI_Isend((void *)message, length, MPI_CHAR, rank, 0, MPI_COMM_WORLD, &isReq);
    message = "bla";
    /**
     * int MPI_Irecv(void *buf, int count, MPI_Datatype datatype, int source,
     *        int tag, MPI_Comm comm, MPI_Request *request)
     */
    MPI_Irecv(&buffer, length, MPI_CHAR, rank, 0, MPI_COMM_WORLD, &irReq);
    printf("Recieve message %s\n", buffer);


    MPI_Cancel(&isReq);
    MPI_Cancel(&irReq);

    MPI_Finalize();
    return 0;
}