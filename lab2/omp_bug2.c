/******************************************************************************
* FILE: omp_bug2.c
* DESCRIPTION:
*   Another OpenMP program with a bug. 
******************************************************************************/
#include <omp.h>

main ()  {

int nthreads, i, tid;
int total;

total = 0;
for (i=0; i<10000; i++) 
     total = total + i;

  printf ("Serial is done! Total= %d\n",total);

/*** Spawn parallel region ***/
#pragma omp parallel
  {
  /* Obtain thread number */
  tid = omp_get_thread_num();
  /* Only master thread does this */
  if (tid == 0) {
    nthreads = omp_get_num_threads();
    printf("Number of threads = %d\n", nthreads);
	
    }
  printf("Thread %d is starting...\n",tid);
 
  #pragma omp barrier

  /* do some work */
 total =0; 
  #pragma omp for schedule(dynamic,10) 
  for (i=0; i<10000; i++) 
     total = total + i;

  printf ("Thread %d is done! Total= %d\n",tid,total);

  } /*** End of parallel region ***/
}
