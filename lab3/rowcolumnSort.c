/******************************************************************************
* FILE: sort.c
* AUTHOR: Cristian Chilipirea
* Generates serially sorts and displays a vector
******************************************************************************/
#include<stdio.h>
#include<stdlib.h>

#define N 5

int sortAsc (const void * a, const void * b) {
	return ( *(int*)a - *(int*)b );
}

int sortDesc (const void * a, const void * b) {
	return ( *(int*)b - *(int*)a );
}

int main(int argc, char argv[]) {
	srand(42);
	int v[N][N];
	int i, j;
	int sorted = 0;
	int aux;

	// generate the vector v with random values
	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
			v[i][j] = rand() % N;

	// sort the matrix v

//	If sortOrder = 1 -> first: asc, second: desc
	int sortOrder = 0, ok = 0;

 	int k;
	for (k = 1; k < N * N; k *= 2) {
		#pragma omp parallel for
		for (i = 0; i < N; i += 2) {
			if (sortOrder) {
				qsort(v[i], N, sizeof(int), sortAsc);
			} else {
				qsort(v[i], N, sizeof(int), sortDesc);
			}
		}

		#pragma omp parallel for
		for (i = 1; i < N; i += 2) {

			if (!sortOrder) {
				qsort(v[i], N, sizeof(int), sortAsc);
			} else {
				qsort(v[i], N, sizeof(int), sortDesc);
			}
		}

		#pragma omp parallel for
		for (i = 0; i < N - 1; i++) {
			for (j = 0; j < N; j++) {
				if (v[i][j] > v[i + 1][j]) {
					aux = v[i][j];
					v[i][j] = v[i + 1][j];
					v[i + 1][j] = aux;
					sorted = 0;
				}
			}
		}

		sortOrder = !sortOrder;
	}

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			printf("%i ", v[i][j]);
		}
		printf("\n");
	}

	// display the vector v
	/*for(i = 0; i < N; i++) {
		if(i%2) {
			for(j = 0; j < N; j++) {
				printf("%i\t", v[i][j]);
				printf("%i\t", v[i][j]);
			}
		} else {
			for(j = N-1; j >= 0; j--) {
				printf("%i\t", v[i][j]);
			}
		}
	}*/
	printf("\n");
}